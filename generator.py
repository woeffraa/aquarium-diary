# -*- coding: utf-8 -*-
import time, locale, string

from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import A5, portrait
from reportlab.lib.units import mm

from datetime import date


class Generator(Canvas):
    JOURNAL_VERSION = 2.1
    JOURNAL_YEAR = 2017
    JOURNAL_AUTHOR = "Woeffray Alain"
    JOURNAL_GIT = "https://bitbucket.org/woeffraa/aquarium-diary"

    CHECKBOX_SIZE = 3 * mm
    LINE_HEIGHT = 5 * mm
    LINE_MARGIN = 1.5 * mm
    MARGIN_ONGLET = 12 * mm
    MARGIN_OUTER_ONGLET = 2 * mm
    MARGIN_PAGE_TITLE = 8.5 * mm
    MARGIN_TABLE_TITLE = 1.5 * mm
    MARGIN_TOP = 10 * mm
    MARGIN_BOTTOM = 4 * mm
    MARGIN_IN = 8 * mm
    MARGIN_OUT = 2 * mm
    MARGIN_TANK_LEFT = 1.5 * mm
    TITLE_LINE_WIDTH = 35 * mm
    TITLE_LINE_HEIGHT = 6 * mm
    TITLE_TABLE_FONT_SIZE = 3 * mm
    TITLE_COLUMN_FONT_SIZE = 2.5 * mm
    TABLE_HEADER_HEIGHT = 4 * mm
    TABLE_HEADER_FONT_SIZE = 2.5 * mm
    TABLE_VALUES_FONT_SIZE = 2.5 * mm
    TABLES_MARGIN = 4 * mm
    CALENDAR_HEADER_HEIGHT = 4 * mm
    CALENDAR_HEADER_FONT_SIZE = 2.5 * mm
    ONGLET_DELTA_Y = 6 * mm
    ONGLET_FONT_SIZE = 2.5 * mm
    ONGLET_NUMBER = 15
    ONGLET_DELTA_Y = 4 * mm
    TANK_SPACE_WIDTH = 75 * mm
    TANK_UNITS_WIDTH = 15 * mm
    TANK_TITLE_WIDTH = 20 * mm
    ENDPAGE_FONT_SIZE = 2 * mm
    ENDPAGE_LINE_HEIGHT = 3 * mm
    ENDPAGE_TEXT_POSITION = 198 * mm
    GRAY_LEVEL = 0.9

    LIST_1D_EMPTY = []
    LIST_2D_EMPTY = [[]]

    """
        @param year Year of the diary
    """
    year = JOURNAL_YEAR

    """
        @param x X coord of the left
        @param y Y coord of the left
        @param width Width of the line
    """

    def textLine(self, x, y, width):

        self.setStrokeColorRGB(self.GRAY_LEVEL, self.GRAY_LEVEL, self.GRAY_LEVEL)
        self.line(x, y, x + width, y)

    """
        @param x1 X coord of start point
        @param y1 Y coord of start point
        @param x2 X coord of end point
        @param y2 Y coord of end point
        @param space Space between lines [mm]
    """

    def makeLimitLine(self, x1, y1, x2, y2, space):

        lineLang = 0.0 * mm
        xConstant = 0.0 * mm
        yConstant = 0.0 * mm

        deltaX = x2 - x1
        deltaY = y2 - y1

        if space <= 0:
            space = 1

        lineLang = (deltaX ** 2 + deltaY ** 2) ** 0.5
        xConstant = deltaX / (lineLang / space)
        yConstant = deltaY / (lineLang / space)

        for i in range(int(lineLang / (space * 2))):

            if i % 2 == 0:
                self.setStrokeColorRGB(self.GRAY_LEVEL, self.GRAY_LEVEL, self.GRAY_LEVEL)
                self.line(float(x1 + i * xConstant * space),
                          float(y1 + i * yConstant * space),
                          float(x1 + (i + 1) * xConstant * space),
                          float(y1 + (i + 1) * yConstant * space))

    """
        @param x X coord of the title's center
        @param y Y coord of the title's center
        @param title Title of the tab
    """

    def makeOngletTitle(self, x, y, title):

        self.saveState()
        self.translate(x, y)
        self.rotate(90)
        self.setFontSize(self.ONGLET_FONT_SIZE)
        self.drawCentredString(0, 0, title)
        self.restoreState()

    """
        @param ongletNb Number of the actual page onglet
        @param title Title of the actual page onglet
    """

    def makeOnglet(self, ongletNb, title):

        ongletHeight = self.pageSize[1] / self.ONGLET_NUMBER

        if ongletNb < (self.ONGLET_NUMBER - 1):
            self.makeLimitLine(self.pageSize[0], ongletHeight * (ongletNb + 1) + self.ONGLET_DELTA_Y / 2,
                               self.pageSize[0] + self.MARGIN_ONGLET,
                               ongletHeight * (ongletNb + 1) - self.ONGLET_DELTA_Y / 2, 2)
            self.makeLimitLine(self.pageSize[0], ongletHeight * (ongletNb + 1) + self.ONGLET_DELTA_Y / 2,
                               self.pageSize[0], self.pageSize[1], 2)

        self.makeOngletTitle(self.pageSize[0] + (self.MARGIN_ONGLET - self.ONGLET_FONT_SIZE / 1.5) / 2 - self.MARGIN_OUTER_ONGLET,
                             ongletHeight * (ongletNb + 0.5), title)

    """
        @param x X coord of the top left
        @param y Y coord of the top left
        @param width Width of the column
        @param values Values list for each line of the column
        @param nbLines Number of lines in the column
    """

    def makeColumn(self, x, y, width, nbLines, values):

        self.rect(x, y, width, self.LINE_HEIGHT * nbLines + self.LINE_MARGIN)

        for i in range(nbLines):
            try:
                values[i]
            except:
                self.textLine(x + self.LINE_MARGIN, y + self.LINE_HEIGHT * (i + 1), width - 2 * self.LINE_MARGIN)
            else:
                if values[i] == "":
                    self.textLine(x + self.LINE_MARGIN, y + self.LINE_HEIGHT * (i + 1), width - 2 * self.LINE_MARGIN)
                else:
                    self.setFontSize(self.TABLE_VALUES_FONT_SIZE)
                    self.drawString(x + self.LINE_MARGIN, y + self.LINE_HEIGHT * (i + 1), values[i])

    """
        @param x X coord of the top left
        @param y Y coord of the top left
        @param header Name of the column
        @param width Width of the column
        @param nbLines Number of lines in the column
        @param values Values list for each line of the column
        @return The X coord of the next column
    """

    def makeColumnWithHeader(self, x, y, header, width, nbLines, values, *gray, **centered):

        self.makeColumn(x, y + self.TABLE_HEADER_HEIGHT, width, nbLines, values)
        if( gray ):
            self.setFillColorRGB(self.GRAY_LEVEL, self.GRAY_LEVEL, self.GRAY_LEVEL)
        else :
            self.setFillColorRGB(1, 1, 1)

        self.setStrokeColorRGB(0, 0, 0)
        self.rect(x, y, width, self.TABLE_HEADER_HEIGHT, True, True)
        self.setFillColorRGB(0, 0, 0)
        self.setFontSize(self.TABLE_HEADER_FONT_SIZE)

        if( centered ):
            self.drawCentredString(x + width/2,
                            y + (self.TABLE_HEADER_HEIGHT + self.TABLE_HEADER_FONT_SIZE / 1.5) / 2,
                            header)
        else:
            self.drawString(x + self.LINE_MARGIN, y + (self.TABLE_HEADER_HEIGHT + self.TABLE_HEADER_FONT_SIZE / 1.5) / 2,
                        header)

        return x + width

    """
        @param x X coord of the top left
        @param y Y coord of the top left
        @param headers Name of each column from left to right
        @param widths Width of each column from left to right
        @param nbLines Number of lines in the table
        @param values Values list for each cell of the table
        @return The Y coord of the table's bottom
    """

    def makeTable(self, x, y, headers, widths, nbLines, values, **centered):

        height = self.TABLE_HEADER_HEIGHT + self.LINE_HEIGHT * nbLines + self.LINE_MARGIN

        for i, header in enumerate(headers):
            try:
                values[i]
            except:
                x = self.makeColumnWithHeader(x, y, header, widths[i], nbLines, self.LIST_1D_EMPTY, centered= centered)
            else:
                x = self.makeColumnWithHeader(x, y, header, widths[i], nbLines, values[i], centered= centered)

        return y + height

    """
        @param x X coord of the top left
        @param y Y coord of the top left
        @param headers Name of each column from left to right
        @param widths Width of each column from left to right
        @param nbLines Number of lines in the table
        @param values Values list for each cell of the table
        @param title Title of the table
        @return The Y coord of the table's bottom
    """

    def makeTableWithTitle(self, x, y, headers, widths, nbLines, values, title, **centered):


        self.setFillColorRGB(self.GRAY_LEVEL, self.GRAY_LEVEL, self.GRAY_LEVEL)
        self.setStrokeColorRGB(0, 0, 0)
        self.rect(x, y, sum(widths), self.TABLE_HEADER_HEIGHT, True, True)
        self.setFillColorRGB(0, 0, 0)
        self.setFontSize(self.TABLE_HEADER_FONT_SIZE)
        self.drawString(x + self.LINE_MARGIN, y + (self.TABLE_HEADER_HEIGHT + self.TABLE_HEADER_FONT_SIZE / 1.5) / 2,
                        title)


        if( centered ):
            yBottom = self.makeTable(x, y + self.TABLE_HEADER_HEIGHT, headers, widths, nbLines, values, centered = centered)
        else:
            yBottom = self.makeTable(x, y + self.TABLE_HEADER_HEIGHT, headers, widths, nbLines, values)

        return yBottom

    """
        @param x X coord of the top left
        @param y Y coord of the top left
        @param width Width of the column
        @param nbLines Number of lines in the field
        @param title Title of the field
    """

    def makeNoteField(self, x, y, width, nbLines, title):

        self.makeColumnWithHeader(x, y, title, width, nbLines, self.LIST_1D_EMPTY, True)

        height = self.TABLE_HEADER_HEIGHT + self.LINE_HEIGHT * nbLines + self.LINE_MARGIN

        return y + height

    """
        @param year Year of the date for test
        @param month Month of the date for test
    """

    def nbDaysInMonth(self, year, month):

        nbDays = 0

        for i in range(1, 32):
            try:
                date(year, month, i)
            except:
                pass
            else:
                nbDays = i

        return nbDays

    """
        @param x X coord of the top left
        @param y Y coord of the top left
        @param height Height of the box
        @param width Width of the box
        @param day Day number in the month
        @return The x and the y coord of the next box
    """

    def makeCalendarBox(self, x, y, height, width, day):

        self.setFillColorRGB(self.GRAY_LEVEL, self.GRAY_LEVEL, self.GRAY_LEVEL)
        self.setStrokeColorRGB(0, 0, 0)
        self.rect(x, y, width, self.CALENDAR_HEADER_HEIGHT, True, True)
        self.setFillColorRGB(0, 0, 0)
        self.setFontSize(self.CALENDAR_HEADER_FONT_SIZE)
        self.drawCentredString(x + width / 2,
                               y + (self.CALENDAR_HEADER_HEIGHT + self.CALENDAR_HEADER_FONT_SIZE / 1.5) / 2, str(day))
        self.rect(x, y + self.CALENDAR_HEADER_HEIGHT, width, height - self.CALENDAR_HEADER_HEIGHT)

        self.setStrokeColorRGB(self.GRAY_LEVEL, self.GRAY_LEVEL, self.GRAY_LEVEL)
        self.rect(x + self.LINE_MARGIN, y + self.CALENDAR_HEADER_HEIGHT + self.LINE_MARGIN, self.CHECKBOX_SIZE, self.CHECKBOX_SIZE)

    """
        @param x X coord of the top left
        @param y Y coord of the top left
        @param boxHeight Height of day's boxes
        @param boxWidth Width of day's boxes
        @param firstDay Day of the week of the first month's day
        @param nbDays Number of days in the month
    """

    def makeCalendar(self, x, y, boxHeight, boxWidth, firstDay, nbDays):

        nbBoxX = 0
        nbBoxY = 0

        for i in range(nbDays):
            if i == 0:
                self.makeCalendarBox(x + ((firstDay - 1) * boxWidth), y, boxHeight, boxWidth, i + 1)
                nbBoxX = firstDay
            else:
                if nbBoxX < 7:
                    self.makeCalendarBox(x + nbBoxX * boxWidth, y + nbBoxY * boxHeight, boxHeight, boxWidth, i + 1)
                    nbBoxX += 1
                else:
                    self.makeCalendarBox(x, y + (nbBoxY + 1) * boxHeight, boxHeight, boxWidth, i + 1)
                    nbBoxX = 1
                    nbBoxY += 1

    def makeTitlePage(self):

        self.drawCentredString(self.pageCenter[0], self.pageCenter[1] - self.TITLE_LINE_HEIGHT,
                               "Journal d'aquarium")
        self.drawCentredString(self.pageCenter[0], self.pageCenter[1], str(self.year))

        for i in range(2):
            self.textLine(self.pageCenter[0] - self.TITLE_LINE_WIDTH / 2,
                          self.pageCenter[1] + self.TITLE_LINE_HEIGHT * (i + 1), self.TITLE_LINE_WIDTH)

        self.makeLimitLine(self.pageSize[0], 0, self.pageSize[0], self.pageSize[1], 2)
        self.showPage()

    def makeTankPage(self):

        nbLines = 38
        titleLine = ""
        unitLine = ""

        x = self.MARGIN_OUT + self.MARGIN_ONGLET
        y = self.MARGIN_TOP

        width = self.pageSize[0] - self.MARGIN_OUT - self.MARGIN_IN
        height = nbLines * self.LINE_HEIGHT + self.LINE_MARGIN

        self.drawString(x, self.MARGIN_PAGE_TITLE, "Bac")

        self.setFillColorRGB(self.GRAY_LEVEL, self.GRAY_LEVEL, self.GRAY_LEVEL)
        self.setStrokeColorRGB(0, 0, 0)
        self.rect(x, y, width, self.TABLE_HEADER_HEIGHT, True, True)
        self.setFillColorRGB(0, 0, 0)

        self.setFontSize(self.TABLE_HEADER_FONT_SIZE)
        self.drawString(x + self.MARGIN_TANK_LEFT,
                        y + (self.TABLE_HEADER_HEIGHT + self.TABLE_HEADER_FONT_SIZE / 1.5) / 2,
                        "Paramètres du bac")

        self.rect(x, y + self.TABLE_HEADER_HEIGHT, width, height, )

        for i in range(nbLines):
            if i < 6 and not i == 3:
                self.textLine(x + self.LINE_MARGIN + self.TANK_TITLE_WIDTH + self.TANK_UNITS_WIDTH,
                              y + self.TABLE_HEADER_HEIGHT + self.LINE_HEIGHT * (i + 1),
                              width - self.TANK_TITLE_WIDTH - self.TANK_UNITS_WIDTH - self.TANK_SPACE_WIDTH)
            elif i > 6:
                self.textLine(x + self.LINE_MARGIN + self.TANK_TITLE_WIDTH,
                              y + self.TABLE_HEADER_HEIGHT + self.LINE_HEIGHT * (i + 1),
                              width - self.TANK_TITLE_WIDTH - self.LINE_MARGIN * 2)

            if i == 0:
                titleLine = "Dimensions"
            elif i == 4:
                titleLine = "Volume"
            elif i == 7:
                titleLine = "Mise en eau" + " :"
            elif i == 9:
                titleLine = "Sol" + " :"
            elif i == 13:
                titleLine = "Substrat" + " :"
            elif i == 17:
                titleLine = "Eclairage" + " :"
            elif i == 21:
                titleLine = "Chauffage" + " :"
            elif i == 25:
                titleLine = "Filtration" + " :"
            else:
                titleLine = ""

            self.setFontSize(self.TITLE_COLUMN_FONT_SIZE)
            self.drawString(x + self.MARGIN_TANK_LEFT, y + self.TABLE_HEADER_HEIGHT + self.LINE_HEIGHT * (i + 1),
                            titleLine)

            if i == 0:
                unitLine = "H [cm]" + " :"
            elif i == 1:
                unitLine = "L [cm]" + " :"
            elif i == 2:
                unitLine = "P [cm]" + " :"
            elif i == 4:
                unitLine = "Brut [L]" + " :"
            elif i == 5:
                unitLine = "Net [L]" + " :"
            else:
                unitLine = ""

            self.setFontSize(self.TITLE_COLUMN_FONT_SIZE)
            self.drawString(x + self.MARGIN_TANK_LEFT + self.TANK_TITLE_WIDTH,
                            y + self.TABLE_HEADER_HEIGHT + self.LINE_HEIGHT * (i + 1), unitLine)

        self.showPage()

    def makeTankConfigPage(self):

        nameColumnWidth = self.pageSize[0] - self.MARGIN_IN / 2 - self.MARGIN_OUT - 80 * mm

        y = self.makeTableWithTitle(self.MARGIN_IN / 2, self.MARGIN_TOP,
                                    ["Nom", "Nbr min. (f/m)", "t°", "d° GH", "pH"],
                                    [nameColumnWidth, 20 * mm, 20 * mm, 20 * mm, 20 * mm], 18, self.LIST_2D_EMPTY,
                                    "Poissons")

        self.makeTableWithTitle(self.MARGIN_IN / 2, y + self.TABLES_MARGIN,
                                ["Nom", "Eclairage", "t°", "d° GH", "pH"],
                                [nameColumnWidth, 20 * mm, 20 * mm, 20 * mm, 20 * mm], 17, self.LIST_2D_EMPTY,
                                "Plantes")

        self.makeOnglet(0, "Bac")
        self.showPage()

    """
        @param month Month for the calendar's page
    """

    def makeCalendarPage(self, month):

        firstDateInMonth = date(self.year, month, 1)
        firstDayInMonth = date.weekday(firstDateInMonth) + 1
        nbDaysInMonth = self.nbDaysInMonth(self.year, month)

        x = self.MARGIN_OUT + self.MARGIN_ONGLET
        y = self.MARGIN_TOP

        locale.setlocale(locale.LC_TIME, "fra_FRa")

        titleName = firstDateInMonth.strftime('%B %Y').capitalize()

        self.drawString(x, self.MARGIN_PAGE_TITLE, titleName)

        self.makeCalendar(x, y, (self.pageSize[1] - self.MARGIN_TOP - self.MARGIN_BOTTOM) / 6,
                          (self.pageSize[0] - self.MARGIN_OUT - self.MARGIN_IN) / 7, firstDayInMonth, nbDaysInMonth)

        self.showPage()

    """
        @param month Month for the calendar's page
    """

    def makeToDoPage(self, month):

        ongletName = ""

        taskColumnWidth = self.pageSize[0] - self.MARGIN_IN / 2 - self.MARGIN_OUT - 30 * mm

        if month == 1:
            ongletName = "Jan."
        elif month == 2:
            ongletName = "Fév."
        elif month == 3:
            ongletName = "Mars"
        elif month == 4:
            ongletName = "Avr."
        elif month == 5:
            ongletName = "Mai"
        elif month == 6:
            ongletName = "Juin"
        elif month == 7:
            ongletName = "Juil."
        elif month == 8:
            ongletName = "Août"
        elif month == 9:
            ongletName = "Sep."
        elif month == 10:
            ongletName = "Oct."
        elif month == 11:
            ongletName = "Nov."
        elif month == 12:
            ongletName = "Déc."
        else:
            ongletName = "Error"

        nbColumnWidth = 10 * mm
        speciesColumnWidth = (self.pageSize[0] - self.MARGIN_IN / 2 - self.MARGIN_OUT - 2 * nbColumnWidth)/2
        causeColumnWidth = self.pageSize[0] - self.MARGIN_IN / 2 - self.MARGIN_OUT - nbColumnWidth - speciesColumnWidth

        headers = ["Nb", "Espèce", "Nb", "Espèce"]
        widths = [nbColumnWidth, speciesColumnWidth, nbColumnWidth, speciesColumnWidth]

        y = self.makeTableWithTitle(self.MARGIN_IN / 2, self.MARGIN_TOP, headers, widths, 4, self.LIST_2D_EMPTY, "Population")

        headers = ["Nb", "Espèce", "Cause"]
        widths = [nbColumnWidth, speciesColumnWidth, causeColumnWidth]

        y = self.makeTableWithTitle(self.MARGIN_IN / 2, y + self.TABLES_MARGIN, headers, widths, 4, self.LIST_2D_EMPTY,
                                    "Transfert")

        y = self.makeNoteField(self.MARGIN_IN / 2, y + self.TABLES_MARGIN,
                               self.pageSize[0] - self.MARGIN_IN / 2 - self.MARGIN_OUT, 7, "Notes")

        self.makeTableWithTitle(self.MARGIN_IN / 2, y + self.TABLES_MARGIN,
                                    ["Date", "Description", "OK"],
                                    [20 * mm, taskColumnWidth, 10 * mm], 15, self.LIST_2D_EMPTY, "A faire")



        self.makeOnglet(month, ongletName)
        self.showPage()

    """
        @param headerPager If true, the header page is generated
    """

    def makeTestPage(self, headerPage):

        pageNumber = 13
        ongletName = "Test"

        valueColumnWidth = self.pageSize[0] - self.MARGIN_IN - self.MARGIN_OUT - 100 * mm
        dateColumnWidth = self.pageSize[0] - self.MARGIN_IN - self.MARGIN_OUT - 110 * mm

        headers = ["Date", "Heure", "t°", "pH", "GH", "KH", "CO2", "NH4", "NO2",
                   "NO3", "Fe", "PO4"]
        widths = [dateColumnWidth, 10 * mm, 10 * mm, 10 * mm, 10 * mm, 10 * mm, 10 * mm, 10 * mm, 10 * mm, 10 * mm,
                  10 * mm, 10 * mm]

        if headerPage:

            x = self.MARGIN_OUT + self.MARGIN_ONGLET

            self.drawString(x, self.MARGIN_PAGE_TITLE, "Qualité de l'eau")

            y = self.makeTableWithTitle(x, self.MARGIN_TOP,
                               ["Valeur", "t°", "pH", "GH", "KH", "CO2", "NH4", "NO2",
                                "NO3", "Fe", "PO4"],
                               [valueColumnWidth, 10 * mm, 10 * mm, 10 * mm, 10 * mm, 10 * mm, 10 * mm, 10 * mm,
                                10 * mm, 10 * mm, 10 * mm], 3,
                               [["Min", "Optimale", "Max"],
                                ["24", "", "28"],
                                ["6.5", "", "8"],
                                ["4", "", "8"],
                                ["5", "", "12"],
                                ["15", "", "60"],
                                ["0", "", "0.02"],
                                ["0", "", "0.1"],
                                ["0", "", "50"],
                                ["0.05", "", "0.2"],
                                ["0", "", "1"]], "Référence", centered=[False, True, True, True, True, True, True])

            self.makeTableWithTitle(x, y + self.TABLES_MARGIN, headers, widths, 31, self.LIST_2D_EMPTY, "Tests")

        else:

            x = self.MARGIN_IN

            self.makeTableWithTitle(x, self.MARGIN_TOP, headers, widths, 37, self.LIST_2D_EMPTY, "Tests")
            self.makeOnglet(pageNumber, ongletName)

        self.showPage()

    """
        @param titlePage If true, the title page is generated
    """

    def makeMaterialPage(self, titlePage):

        pageNumber = 14
        ongletName = "Mat."
        priceColumnWidth = 15 * mm

        productColumnWidth = self.pageSize[0] - self.MARGIN_IN - self.MARGIN_OUT - 85 * mm
        headers = ["Date", "Magasin", "Marque", "Produit", "Qté", "Prix"]
        widths = [15 * mm, 20 * mm, 20 * mm, productColumnWidth, 15 * mm, priceColumnWidth]

        if titlePage:

            x = self.MARGIN_OUT + self.MARGIN_ONGLET

            y = self.makeTableWithTitle(x, self.MARGIN_TOP, headers, widths, 36, self.LIST_2D_EMPTY, "Matériel")
        else:

            x = self.MARGIN_IN

            y = self.makeTableWithTitle(x, self.MARGIN_TOP, headers, widths, 36, self.LIST_2D_EMPTY, "Matériel")

            self.makeOnglet(pageNumber, ongletName)

        self.setFontSize(self.TITLE_COLUMN_FONT_SIZE)
        self.drawString(x + self.LINE_MARGIN, y + self.LINE_HEIGHT, "Total" + " :")

        self.setStrokeColorRGB(0, 0, 0)
        self.makeColumn(x + self.pageSize[0] - self.MARGIN_IN - self.MARGIN_OUT - priceColumnWidth, y, priceColumnWidth,
                        1, self.LIST_1D_EMPTY)

        self.showPage()

    def makeEndPage(self):

        text = ["Journal d'aquarium" + " v" + str(self.JOURNAL_VERSION),
                self.JOURNAL_GIT,
                "Imprimé le" + " " + time.strftime("%d.%m.%Y")]

        for i in range(3):
            self.setFontSize(self.ENDPAGE_FONT_SIZE)
            self.drawCentredString(self.pageCenter[0] + self.MARGIN_ONGLET / 2,
                                   self.ENDPAGE_TEXT_POSITION + i * self.ENDPAGE_LINE_HEIGHT, text[i])

    def __init__(self):
        FILE = "Journal.pdf"
        Canvas.__init__(self, FILE, pagesize=portrait(A5), bottomup=0)
        Canvas.setTitle(self, FILE)

        self.pageSize = [A5[0] - self.MARGIN_ONGLET, A5[1]]
        self.pageCenter = [self.pageSize[0] / 2, self.pageSize[1] / 2]

        self.makeTitlePage()
        self.makeTankPage()
        self.makeTankConfigPage()

        for i in range(12):
            self.makeCalendarPage(i + 1)
            self.makeToDoPage(i + 1)

        self.makeTestPage(True)
        self.makeTestPage(False)
        self.makeMaterialPage(True)
        self.makeMaterialPage(False)
        self.makeEndPage()


c = Generator()
c.showPage()
c.save()
